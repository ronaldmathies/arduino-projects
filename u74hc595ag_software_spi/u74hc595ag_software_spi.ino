volatile byte  anodeIndex = 0;
volatile byte anodeLevel[] = {
              B11111110,      
              B11111101,      
              B11111011, 
              B11110111, 
              B11101111, 
              B11011111, 
              B10111111,
              B01111111};

#if defined(ESP8266)
  // 595 Wiring
  //
  // |- Function -|- Wire -|- ESP8266 -|- 595 PIN  -|
  // | LATCH      | GREEN  | D3 GPIO0  | 12 (RCLK)  | 
  // | BLANK      | BLUE   | D8 GPIO15 | 13 (OE)    |
  // | CLOCK      | PURPLE | D4 GPIO2  | 11 (SRCLK) |
  // | MOSI       | YELLOW | D2 GPIO4  | 14 (SER)   |
  #define HC595_LATCH D3
  #define HC595_BLANK D8
  
  #define SPI_MOSI    D2
  #define SPI_CLOCK   D4
#endif

void setup() {
  Serial.begin(115200);

  pinMode(HC595_LATCH, OUTPUT);
  pinMode(HC595_BLANK, OUTPUT);
  
  pinMode(SPI_MOSI,  OUTPUT);
  pinMode(SPI_CLOCK, OUTPUT);
}

void loop() {
  #if defined(ESP8266)
    digitalWrite(HC595_BLANK, HIGH);
    digitalWrite(HC595_LATCH, LOW); 
  #endif

  shiftOut(SPI_MOSI, SPI_CLOCK, MSBFIRST, anodeLevel[0]);
  anodeIndex = (anodeIndex + 1) % 8; // increment anode index ready for next ISR

  #if defined(ESP8266)
    digitalWrite(HC595_LATCH, HIGH);
    digitalWrite(HC595_BLANK, LOW);
  #endif
  
  delay(50);
}
