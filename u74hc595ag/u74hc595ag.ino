#include <SPI.h>

volatile byte  anodeIndex = 0;
volatile byte anodeLevel[] = {
              B11111110,      
              B11111101,      
              B11111011, 
              B11110111, 
              B11101111, 
              B11011111, 
              B10111111,
              B01111111};

#if defined(__AVR_ATmega328P__)
  // 595 Wiring
  //
  // |- Function -|- Wire -|- ATMEGA PIN -|- UNO PIN -|- 595 PIN  -|- Arduino Port A -|- Arduino Port B -|
  // | LATCH      | GREEN  | 14           | 8         | 12 (RCLK)  | 
  // | BLANK      | BLUE   | 15           | 9         | 13 (OE)    |
  // | CLOCK      | PURPLE | 19           | 13        | 11 (SRCLK) |
  // | MOSI       | YELLOW | 17           | 11        | 14 (SER)   |
  #define HC595_LATCH 8
  #define HC595_BLANK 9
  
  #define SPI_MOSI    11
  #define SPI_CLOCK   13
  
  #define HC595_LATCH_PORTB 0  // LATCH ON PORTB
  #define HC595_BLANK_PORTB 1  // BLANK ON PORTB
#elif defined(ESP8266)
  // 595 Wiring
  //
  // |- Function -|- Wire -|- ESP8266 -|- 595 PIN  -|
  // | LATCH      | GREEN  | D4        | 12 (RCLK)  | 
  // | BLANK      | BLUE   | D3        | 13 (OE)    |
  // | CLOCK      | PURPLE | D5        | 11 (SRCLK) |
  // | MOSI       | YELLOW | D7        | 14 (SER)   |
  #define HC595_LATCH D4
  #define HC595_BLANK D3
  
  #define SPI_MOSI    D7
  #define SPI_CLOCK   D5
#endif

void setup() {
  Serial.begin(9600);

  pinMode(HC595_LATCH, OUTPUT);
  pinMode(HC595_BLANK, OUTPUT);
  
  pinMode(SPI_MOSI,  OUTPUT);
  pinMode(SPI_CLOCK, OUTPUT);
  
  //Set up the SPI
  SPI.setBitOrder(MSBFIRST);           // Most Significant Bit First
  SPI.setDataMode(SPI_MODE0);          // Mode 0 Rising edge of data, keep clock low
  SPI.setClockDivider(SPI_CLOCK_DIV2); // Run the data in at 16MHz/2 - 8MHz (max speed)

  SPI.begin();
}

void loop() {
  #if defined(__AVR_ATmega328P__)
    PORTB |= 1 << HC595_BLANK_PORTB;       // Blank high
  #elif defined(ESP8266)
    digitalWrite(HC595_BLANK, HIGH);
  #endif
  
  SPI.transfer(anodeLevel[anodeIndex]);  // send the anode multiplex byte
  anodeIndex = (anodeIndex + 1) % 8;     // incriment anode index ready for next ISR

  #if defined(__AVR_ATmega328P__)
    PORTB |=  (1 << HC595_LATCH_PORTB);    // Latch high
    PORTB &= ~(1 << HC595_LATCH_PORTB);    // Latch low
    PORTB &= ~(1 << HC595_BLANK_PORTB);    //Blank goes LOW to start the next cycle
  #elif defined(ESP8266)
    digitalWrite(HC595_LATCH, HIGH);
    digitalWrite(HC595_LATCH, LOW);
    digitalWrite(HC595_BLANK, LOW);
  #endif
  delay(100);
}
