#include <SPI.h>

#define ENABLE_EXPERIMENTAL  false

// Cube dimensions
#define CUBE_WIDTH    8
#define CUBE_HEIGHT   8
#define CUBE_DEPTH    8

#if ENABLE_EXPERIMENTAL
  // Each led has a bit depth of 8, which results in a 0-254 brightness, 
  // higher results in memory issues. When the value is adjusted additional
  // changed need to be made in various methods.
  #define LED_BIT_DEPTH 8
#else
  // Each led has a bit depth of 1, which results in a 0 or 1 for full brightness,
  // this is the minimum amounth of bit depth possible and consumes the least amount of
  // memory
  #define LED_BIT_DEPTH 1
#endif

// For normal leds use 1, for RGB leds use 3
#define LED_SUB_COUNT 3

#define LED_VALUE_DATA_SIZE_LAYER (CUBE_WIDTH * CUBE_DEPTH * LED_BIT_DEPTH) / 8

// TLC5940 chipset specifics
#define TLC_COUNT         12
#define TLC_CHANNEL_COUNT 16
#define TLC_BIT_DEPTH     12

// Calculate the size of the SPI Buffer
#define SPI_BUFFER_SIZE (TLC_COUNT * TLC_CHANNEL_COUNT * TLC_BIT_DEPTH) / 8

// TLC Wiring
//
// |- Function -|- Wire -|- ATMEGA PIN -|- UNO PIN -|- TLC PIN  -|
// | XLAT       | GREEN  | 4            | 2         | 24 (XLAT)  | 
// | GSCLK      | WHITE  | 5            | 3         | 18 (GSCLK) | 
// | VPRG       | GRAY   | 6            | 4         | 27 (VPRG)  |
// | BLANK      | BLUE   | 11           | 5         | 23 (BLANK) |
// | SIN        | YELLOW | 19           | 11        | 26 (SIN)   |
// | CLOCK      | PURPLE | 17           | 13        | 25 (SCLKL) |
#define TLC_XLAT  2
#define TLC_GSCLK 3 // Oscillated by timer2 at 8Mhz ( 1s / 8Mhz = 125ns, 125ns * 4096 = 512ms)
#define TLC_VPRG  4
#define TLC_BLANK 5

#define SPI_MOSI  11
#define SPI_CLOCK 13

#define SPI_CLOCK_PORTB 5  // SPI CLOCK ON PORTB

// Timing measuring pins for the oscilloscope.
#define LOOP_MEASURE_PIN  6
#define TIMER_MEASURE_PIN 7

volatile byte anodeIndex = 0;

// Contains the brightness for each individual led, each value is 
// stored as bits (LED_BIT_DEPTH)
uint8_t ledValue[CUBE_HEIGHT][LED_SUB_COUNT][LED_VALUE_DATA_SIZE_LAYER];

// The SPI buffer is used to temporarilty store the data that needs to be transmitted over the SPI bus.
// The size of the buffer is explained above (see SPI_BUFFER_SIZE)
uint8_t spiBuffer[SPI_BUFFER_SIZE];

int sizeOfBuffer = sizeof(spiBuffer);
int spiBufferPos = 0;

void setup() {
  Serial.begin(115200);

  pinMode(TLC_XLAT, OUTPUT);
  pinMode(TLC_GSCLK, OUTPUT);
  pinMode(TLC_VPRG, OUTPUT);

  pinMode(SPI_MOSI,  OUTPUT);
  pinMode(SPI_CLOCK, OUTPUT);

  pinMode(TIMER_MEASURE_PIN, OUTPUT);
  pinMode(LOOP_MEASURE_PIN, OUTPUT);
  
  //Set up the SPI
  SPI.setBitOrder(MSBFIRST);           // Most Significant Bit First
  SPI.setDataMode(SPI_MODE0);          // Mode 0 Rising edge of data, keep clock low
  SPI.setClockDivider(SPI_CLOCK_DIV2); // Run the data in at 16MHz/2 - 8MHz (max speed)

  // Set initial led values all to 0.
  clearLedValue();

  // Disable interrupts so we can setup the timers.
  noInterrupts();

  // Timer documentation:
  // https://www.arduino.cc/en/Tutorial/SecretsOfArduinoPWM

  // Configure timer 2 to toggle pin 5 @ 8Mhz, (Arduino Digital 3)
  TCCR2A=B00000000;
  TCCR2A |= (1 << WGM21);

  // |- COM2B1 -|- COM2B0 -|- Description                             -|
  // |     0    |     0    | Normal port operation, OC2B disconnected. |
  // |     0    |     1    | Toggle OC2B on Compare Match.             |
  // |     1    |     0    | Clear OC2B on Compare Match.              |
  // |     1    |     1    | Set OC2B on Compare Match                 |  
  TCCR2A |= (1 << COM2B0);
  //OLD: TCCR2A=B00010010;//Timer 2 set to Compare Mode Toggling pin 5 @ 8MHz, Arduino Digital 3
  
  //Timer 2 prescaler set to 1, 16/1=16 MHz, but toggles pin 5 every other cycle, 8MHz
  TCCR2B=B00000001;

  // Documentation: 13.11.1 TCCR1A – Timer/Counter1 Control Register A
  TCCR1A=B00000000; //Timer 1 doesn't toggle anything, used for counting
  //Timer 1 prescaler set to Fclk/256
  //Why? We need to count 4096 pulses out of Timer 2 - pin 5
  //8 MHz = 1 pulse every 125ns - - 4096 pulses would need 512us
  //Timer 1 runs at 16MHz/256=62.5kHz, we need a match at every 512us
  //Basically, I can get an interrupt to get called every 512us, so...
  // I need to run Timer 2 @ 8MHz for 512us to get 4096 pulses
  // I can't count those pulses directy (too fast) , so
  // I'll count using Timer 1, which makes a count every 16us
  // The counter starts at 0, so we'll set it to 31 to get an interrupt after 512us

  // Documentation: 13.11.2 TCCR1B – Timer/Counter1 Control Register B
  TCCR1B=B00001100;//Mode=CTC with OSCR1A = TOP and 256 as the prescaler
  // Mask set up, will call ISR (Inerrupt Service Routine) for Compare match on A
  TIMSK1=B00000010;
  //These are the match values for the counters
  // 0 here means it will match on one cycle of the clock/prescaler
  OCR1A= 60;//to get our 968us Interrupt
  interrupts();// kick off the timers!

  pinMode(TLC_BLANK, OUTPUT);
}

/**
 * Clears out the ledValue buffer complety by filling it with zeros.
 */
void clearLedValue(){
  memset(&ledValue[0], 0, sizeof(ledValue));
}

ISR(TIMER1_OVF_vect){}// Over Limit Flag Interrupt  you need this even if you don't use it
ISR(TIMER1_COMPB_vect){ }// Compare B - Not Used
ISR(TIMER1_COMPA_vect){ // Interrupt to count 4096 Pulses on GSLCK
  PORTD |= 1 << TIMER_MEASURE_PIN; // write timer measure pin high for the ossciloscope.
  
  PORTD |= 1 << TLC_BLANK;    // write blank HIGH to reset the 4096 counter in the TLC
  PORTD |= 1 << TLC_XLAT;     // write XLAT HIGH to latch in data from the last data stream

  PORTD &= ~(1 << TLC_XLAT);  //XLAT can go low now
  PORTD &= ~(1 << TLC_BLANK); //Blank goes LOW to start the next cycle

  SPI.end();
  SPI.begin();

  // The TLC5940 chips are daisy chained, which means that the first bit of data is
  // send out to the last TLC5940 chip first. So when sending out the data we need
  // to process the data in reverse order. Hence the spiBufferPos starts at the end of 
  // the buffer and works it way to the beginning while reading the data from the ledValue
  // from beginning to the end.
  spiBufferPos = SPI_BUFFER_SIZE;

  #if ENABLE_EXPERIMENTAL

    // Fill the buffer with the data we need to send using the SPI bus. 
    // The ledvalue consists of a one byte value (0-255) so first convert it from 8bits (byte) to 12 bits (0-4095) (int) 
    // which consists of two bytes, only the twelve least significant bits are transmitted. So
    // fill the buffer (which represents all the outputs of all the TLC's) with the converted values wheareas
    // the last 12 bits of the buffer is the first output of the first TLC.
    boolean first = true;
    int data;
    for(; idx < endIdx; idx++) {
  
      // Shifting the value by 4 is faster then multiplying by 16
      data = ledValue[idx] << 4;

      // Get the lower 8 bits (lower byte)
      byte low = data & 0xff;

      if (first) {
        // first value
        spiBuffer[spiBufferPos] = low;
        spiBuffer[--spiBufferPos] = data >> 8;
        first = false;
      } else {
        // second value
        spiBuffer[spiBufferPos] = spiBuffer[spiBufferPos] | low << 4;
        spiBuffer[--spiBufferPos] =  data >> 4 | low >> 4;
        spiBufferPos--;
        first = true;
      }
    }
  #else
    // Fill the buffer with the data we need to send using the SPI bus. 
    // The ledvalue consists of a one byte value (0 or 1) so first we determine if the light is on or off, when on we
    // assume that the led will be on full brightness. Since the TLC has a PWM range of 0 to 4095 we use 4095.
    // Since every channel has 12 bits we need to split data over two bytes where on of the bytes contain information about
    // two led's, so keep this in mind. So fill the buffer (which represents all the outputs of all the TLC's) with
    // the converted values wheareas the last 12 bits of the buffer is the first output of the first TLC.
    bool cValue, nValue;
    uint8_t rowIdx, idx, row;
    for(int8_t colorIdx = 0; colorIdx < LED_SUB_COUNT; colorIdx++) {
      for (rowIdx = 0; rowIdx < LED_VALUE_DATA_SIZE_LAYER; rowIdx++) {
        row = ledValue[anodeIndex][colorIdx][rowIdx];
        for (idx = 0; idx < 8; idx++) {
          cValue = row & (1 << idx);
          nValue = row & (1 << ++idx);

          spiBuffer[--spiBufferPos] = cValue ? 0xFF : 0x00;
          spiBuffer[--spiBufferPos] = cValue ? 0x0F : 0x00 | nValue ? 0xF0 : 0x00;
          spiBuffer[--spiBufferPos] = nValue ? 0xFF : 0x00;
        }
      }
    }
  #endif

  SPI.transfer(&spiBuffer, sizeOfBuffer);

  PORTD &= ~(1 << TIMER_MEASURE_PIN); // write timer measure pin low for the ossciloscope.
}


// Trial... 
void serialEventRun() {}

bool isOn = false;

void loop() {
  PORTD |= 1 << LOOP_MEASURE_PIN; // write loop measure pin high for the ossciloscope.

  #if ENABLE_EXPERIMENTAL
    if (isOn) {
      for (int layerIdx = 0; layerIdx < 8; layerIdx++) {
        for (int colorIdx = 0; colorIdx < 3; colorIdx++) {
          for (int idx = 0; idx < 64; idx += 8) {
            ledValue[layerIdx][colorIdx][idx - 1] = 255;
            ledValue[layerIdx][colorIdx][idx] = 255;
          }
        }
      }
    } else {
      for (int layerIdx = 0; layerIdx < 8; layerIdx++) {
        for (int colorIdx = 0; colorIdx < 3; colorIdx++) {
          for (int idx = 0; idx < 64; idx += 8) {
            ledValue[layerIdx][colorIdx][idx - 1] = 0;
            ledValue[layerIdx][colorIdx][idx] = 0;
          }
        }
      }
    }
  #else
    if (isOn) {
      for (int layerIdx = 0; layerIdx < 8; layerIdx++) {
        for (int colorIdx = 0; colorIdx < 3; colorIdx++) {
          for (int idx = 0; idx < 8; idx++) {
            bitSet(ledValue[layerIdx][colorIdx][idx], 0);
            bitSet(ledValue[layerIdx][colorIdx][idx], 7);
          }
        }
      }
    } else {
      for (int layerIdx = 0; layerIdx < 8; layerIdx++) {
        for (int colorIdx = 0; colorIdx < 3; colorIdx++) {
          for (int idx = 0; idx < 8; idx++) {
            bitClear(ledValue[layerIdx][colorIdx][idx], 0);
            bitClear(ledValue[layerIdx][colorIdx][idx], 7);
          }
        }
      }
    }
  #endif

  isOn = !isOn;

  PORTD &= ~(1 << LOOP_MEASURE_PIN); // write loop measure pin low for the ossciloscope.

  delay(1000);
  
}

void printCube() {
  for (int y = 0; y < CUBE_HEIGHT; y++) {
    for (uint8_t subLed = 0; subLed < LED_SUB_COUNT; subLed++) {
        #if LED_BIT_DEPTH == 1
          for (int row = 0; row < CUBE_WIDTH; row++) {
            printBitsByte(ledValue[y][subLed][row]);
            Serial.println();
          }
        #endif
        Serial.println();
    }
  }
}

void printSpiBuffer(uint8_t spiBuffer[]) {
  for (int a = 0; a < SPI_BUFFER_SIZE; a++) {
    printBitsByte(spiBuffer[a]);
    Serial.print(" - ");
  }
  Serial.println();
}

void printBitsByte(byte n) {
  byte numBits = 8;  // 2^numBits must be big enough to include the number n
  char b;
  char c = ' ';   // delimiter character
  for (byte i = 0; i < numBits; i++) {
    // shift 1 and mask to identify each bit value
    b = (n & (1 << (numBits - 1 - i))) > 0 ? '1' : '0'; // slightly faster to print chars than ints (saves conversion)
    Serial.print(b);
    if (i < (numBits - 1) && ((numBits-i - 1) % 4 == 0 )) Serial.print(c); // print a separator at every 4 bits
  }
}

void printBitsInt(long int n) {
  byte numBits = 16;  // 2^numBits must be big enough to include the number n
  char b;
  char c = ' ';   // delimiter character
  for (byte i = 0; i < numBits; i++) {
    // shift 1 and mask to identify each bit value
    b = (n & (1 << (numBits - 1 - i))) > 0 ? '1' : '0'; // slightly faster to print chars than ints (saves conversion)
    Serial.print(b);
    if (i < (numBits - 1) && ((numBits-i - 1) % 4 == 0 )) Serial.print(c); // print a separator at every 4 bits
  }
}
