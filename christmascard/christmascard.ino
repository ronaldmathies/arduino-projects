#include <SPI.h>

// Burn bootloader:
//
// Initialy the Atmega8 does not have a booloader (at least the TQFP package).
// So we first need to burn the bootloader after which we can burn this sketch.
//
// 1) Install the ArduinoISP sketch anto the Arduino UNO: 
//    File -> Examples -> 11.ArduinoISP -> ArduinoISP 
//
// 2) Change programmer to Arduino as ISP
//    Tools -> Programmer -> "Arduino as ISP"
//
// 3) Switch Board to ATmega8
//    Tools -> Board -> Atmega8
//    Tools -> Clock -> 16 Mhz External
//
// 4) Use following wiring diagram
//
//    |- Function -|- Wire -|- ATMEGA8 PIN -|- Arduino Uno  -|
//    | VCC        | RED    | 4,6           | 5V (VCC)       | 
//    | MOSI       | BLUE   | 15            | 11 (MOSI)      |
//    | MISO       | YELLOW | 16            | 12 (MISO)      |
//    | SCLK       | PURPLE | 17            | 13 (SCK)       |
//    | RESET      | BROWN  | 29            | 10 (SSN)       |
//    | GROUND     | BLACK  | 21,5,3        | GND (GND)      |
//
// 5) Connect Arduino UNO to PC and choose appropriate port
//    Tools -> Port 
//
// 6) Burn bootloader
//    Tools -> Burn Bootloader

// Upload Sketch:
//
// 1. Set programmer to AVRISP mkII
//    Tool -> Programmer -> "AVRISP mkII"
//
// 2) Switch Board to ATmega8 (if not already done for burning the bootloader)
//    Tools -> Board -> Atmega8
//    Tools -> Clock -> 16 Mhz External
// 
// 3) Use the following wiring diagram:
//
//    |- Function -|- Wire -|- ATMEGA8 PIN -|- FTDI  -|
//    | VCC        | RED    | 4,6           | 5V      | 
//    | TX         | BLUE   | 31            | RX      |
//    | RX         | YELLOW | 30            | TX      |
//    | RESET      | BROWN  | 29            | DTR     |
//    | GROUND     | BLACK  | 21,5,3        | GND     |
//
// 5) Connect FTDI to PC and choose appropriate port
//    Tools -> Port 
//
// 6) Upload sketch
//    Sketch -> Upload
//
// In case of the following error:
//
// avrdude: stk500_recv(): programmer is not responding
// avrdude: stk500_getsync() attempt 1 of 10: not in sync: resp=0x00    
//
// Try adding a ceramic capacitor of 100nF / 0.1uF between the reset pins.

// TLC5947 chipset specifications
#define TLC_COUNT          1
#define TLC_CHANNEL_COUNT  24
#define TLC_BIT_DEPTH      12

// Data size for the leds.
#define LED_VALUE_SIZE TLC_COUNT * TLC_CHANNEL_COUNT

// Calculate the size of the SPI Buffer
#define SPI_BUFFER_SIZE (TLC_COUNT * TLC_CHANNEL_COUNT * TLC_BIT_DEPTH) / 8

// TLC Wiring
//
// |- Function -|- Wire -|- ATMEGA8 PIN -|- TLC PIN  -|
// | XLAT       | GREEN  | 6             | 24 (XLAT)  | 
// | BLANK      | BLUE   | 7             | 23 (BLANK) |
// | SIN        | YELLOW | 14            | 26 (SIN)   |
// | CLOCK      | PURPLE | 17            | 25 (SCLKL) |
#define TLC_BLANK 7
#define TLC_XLAT  6
#define SPI_CLOCK 17
#define SPI_MOSI  15

#define SPI_CLOCK_PORTB 5  // SPI CLOCK ON PORTB

// A1 = ADC1 (pin 24)
// A2 = ADC2 (pin 25)
#define BTN_LEFT  A1
#define BTN_RIGHT A2

// Used for software timer purposes.
unsigned long startMillis = 0;

// State of the left and right button, when true the button is pressed.
bool buttonLeftDown = false;
bool buttonRightDown = false;

// Contains the brightness for each individual led, each value is 
// stored as bits (LED_BIT_DEPTH)
volatile uint8_t ledValue[LED_VALUE_SIZE];

// The indices in the ledValue array of the start from left to right.
#define sizeOfStarIndices 15
volatile uint8_t starIndices[sizeOfStarIndices] = {15, 17, 18, 13, 16, 19, 20, 23, 6, 4, 8, 10, 3, 5, 7};

// The indices in the ledValue array of the tree lights from the top and then clockwise.
#define sizeOfTreeIndices 9
volatile uint8_t treeIndices[sizeOfTreeIndices] = {2, 1, 0, 9, 11, 12, 14, 21, 22};

// The SPI buffer is used to temporarilty store the data that needs to be transmitted over the SPI bus.
// The size of the buffer is explained above (see SPI_BUFFER_SIZE)
uint8_t spiBuffer[SPI_BUFFER_SIZE];

// SPI Configuration
//
// Maximum speed:
// The frequency at which the data is transmitted, (Mhz), the maximum in 
// case of a single TLC5947 is 30-Mhz, when cascaded the maximum is 50%.
//
// Data output order:
// The data output order for the TLC5947 is according to the datasheet most significat bit first.
//
// Mode configuration:
// For the TLC5947 the SPI mode is 0, data is clocked in at the rising edge of the clock.
SPISettings TLC5947(16e6 , MSBFIRST, SPI_MODE0);

// Tree animation setting for the current playing animation and the step within the current animation.
uint8_t treeCurrentAnimation = 0;
int8_t treeCurrentAnimationStep = -100;

// Speed at which the animation will play (ns delay).
uint16_t animationDelay = 512; 

// Brightness of the LED's (division factor on the PWM signal).
uint8_t ledBrightness = 2;

// Animations
const uint8_t treeVerticalPatternStepCount = 5;
const uint8_t treeVerticalIndiceCount = 2;
const int8_t treeVerticalPattern[treeVerticalPatternStepCount][treeVerticalIndiceCount] = {
  {0, -1}, {8, 1}, {7, 2}, {6, 3}, {5, 4}
};

const uint8_t treeHorizontalPatternStepCount = 9;
const uint8_t treeHorizontalIndiceCount = 1;
const int8_t treeHorizontalPattern[treeHorizontalPatternStepCount][treeHorizontalIndiceCount] = {
  {5}, {6}, {7}, {8}, {0}, {1}, {2}, {3}, {4}
};

const uint8_t treeVerticalInsideOutPatternStepCount = 3;
const uint8_t treeVerticalInsideOutIndiceCount = 4;
const int8_t treeVerticalInsideOutPattern[treeVerticalInsideOutPatternStepCount][treeVerticalInsideOutIndiceCount] = {
  {0, 5, 4, -1}, {8, 1, 6, 3}, {7, 2, -1, -1}
};

const uint8_t treeAllGreensPatternStepCount = 1;
const uint8_t treeAllGreensIndiceCount = 5;
const int8_t treeAllGreensPattern[treeAllGreensPatternStepCount][treeAllGreensIndiceCount] = {
  {0, 2, 4, 5, 7}
};

const uint8_t treeAllRedsPatternStepCount = 1;
const uint8_t treeAllRedsIndiceCount = 4;
const int8_t treeAllRedsPattern[treeAllRedsPatternStepCount][treeAllRedsIndiceCount] = {
  {1, 3, 6, 8}
};

const uint8_t treeRandomPatternStepCount = 9;
const uint8_t treeRandomIndiceCount = 1;
const int8_t treeRandomPattern[treeRandomPatternStepCount][treeRandomIndiceCount] = {
  {6}, {1}, {4}, {0}, {5}, {2}, {8}, {3}, {7}
};

enum Direction {
  forward, reverse
};

enum OnOff {
  on, off, _60, _20, none
};

void setup() {
  Serial.begin(115200);
  analogReference(DEFAULT);
  randomSeed(analogRead(A0));

  // Set the pinmodes for all neccesary pins, XLAT must be LOW initially (is only used to
  // latch in the new data). CLOCK and MOSI are done through the SPI.begin() operation.
  pinMode(TLC_XLAT, OUTPUT);
  PORTD &= ~(1 << TLC_XLAT);

  // Initialize the led values to 0.
  for (uint8_t idx = 0; idx < 24; idx++) {
    ledValue[idx] = 0;
  }
  
  // Disable timer interrupts.
  noInterrupts();

  // Setup the timer for sending the LED data to the TLC4947 IC's using SPI.
  // Set TCCR1A and TCCR1B register to 0
  TCCR1A = 0;
  TCCR1B = 0;

  // Initialize counter value to 0
  TCNT1  = 0;
  
  // Set compare match register for 1953.125 Hz increments ( 512us )
  OCR1A = 8191; // = 16000000 / (1 * 1953.125) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12, CS11 and CS10 bits for 1 prescaler
  TCCR1B |= (0 << CS12) | (0 << CS11) | (1 << CS10);
  // enable timer compare interrupt
  TIMSK |= (1 << OCIE1A);

  // Enable timer interrupts.
  interrupts();

  pinMode(TLC_BLANK, OUTPUT);
  PORTD &= ~(1 << TLC_BLANK);

  pinMode(BTN_LEFT, INPUT);
  pinMode(BTN_RIGHT, INPUT);

  // Initialize SPI.
  SPI.begin();

  startMillis = millis();
}

void leftButtonPressed() {
  animationDelay *= 2;
  if (animationDelay > 2048) {
    animationDelay = 128;
  }
}

void rightButtonPressed() {
  ledBrightness += 1;
  if (ledBrightness > 4) {
    ledBrightness = 1;
  }
}

ISR(TIMER1_COMPA_vect) {
  // Fill the buffer with the data we need to send using the SPI bus. 
  // The ledvalue consists of a one byte value (0-255) so first convert it from 8bits (byte) to 12 bits (0-4095) (int) 
  // which consists of two bytes, only the twelve least significant bits are to be transmitted (since each output has a
  // 12 bit PWM signal).

  uint16_t data;
  uint16_t spiBufferPos = SPI_BUFFER_SIZE - 1; 
  int8_t first = true;
  for (int8_t idx = 0; idx < LED_VALUE_SIZE; idx++) {
    
    // We store the LED data as 8 bit values for convenience, but the TLC5947 expects 12bits
    // per output channel. So shift left 4 times to get a 16bit value (of which we will only use
    // the 12 least significant bits).
    data = ledValue[idx] << ledBrightness;

    // Compensation for very bright white LED's
    if (idx == 15 || idx == 16 || idx == 18 || idx == 23 || idx == 5) {
      data = data / 4;
    }

    // Get the 8 least significant bits. 
    byte low = data & 0xff;

    if (first) {
      // first value
      spiBuffer[spiBufferPos] = low;
      spiBuffer[--spiBufferPos] = data >> 8;
      first = false;
    } else {
      // second value
      spiBuffer[spiBufferPos] = spiBuffer[spiBufferPos] | low << 4;
      spiBuffer[--spiBufferPos] = data >> 4 | low >> 4;
      spiBufferPos--;
      first = true;
    }
  }
  
  // Submit data
  SPI.beginTransaction(TLC5947);
  SPI.transfer(&spiBuffer, SPI_BUFFER_SIZE);
  SPI.endTransaction();

  // Clock needs to be pulled low at the end of data
  // data transmission. Not sure why, datasheet
  // states that data on the SIN is clocked in on the rising edge and
  // appears on the SOUT on the falling edge.
  PORTB &= ~(1 << SPI_CLOCK_PORTB);

  // Latch in the data by pulling blank high, xlat high, xlat low and blank low.
  PORTD |= 1 << TLC_BLANK;
  PORTD |= 1 << TLC_XLAT;
  PORTD &= ~(1 << TLC_XLAT);
  PORTD &= ~(1 << TLC_BLANK);
}

unsigned long starGlowperiod = 10;
unsigned long currentMillis = 0;

void loop() {
  currentMillis = millis();

  // Handle the left button.
  uint16_t btnValue = analogRead(BTN_LEFT);
  if (btnValue > 512 && buttonLeftDown == false) {
      buttonLeftDown = true;
      leftButtonPressed();
  } else if (btnValue < 512) {
    buttonLeftDown = false;
  }

  // Handle the right button
  uint16_t btnValue1 = analogRead(BTN_RIGHT);
  if (btnValue1 > 512 && buttonRightDown == false) {
      buttonRightDown = true;
      rightButtonPressed();
  } else if (btnValue1 < 512) {
    buttonRightDown = false;
  }
  
  if (currentMillis - startMillis >= starGlowperiod) {
    glowStars();
    startMillis = currentMillis;
  }
  
  switch (treeCurrentAnimation) {
    case 0:
      if (animateTree(forward, on, treeVerticalPatternStepCount, treeVerticalIndiceCount, treeVerticalPattern)) 
        treeCurrentAnimation++; 
      break;
    case 1:
      if (animateTree(forward, off, treeHorizontalPatternStepCount, treeHorizontalIndiceCount, treeHorizontalPattern)) 
        treeCurrentAnimation++; 
      break;
    case 2:
      if (animateTree(reverse, on, treeVerticalPatternStepCount, treeVerticalIndiceCount, treeVerticalPattern)) 
        treeCurrentAnimation++; 
      break;
    case 3:
      if (animateTree(reverse, off, treeHorizontalPatternStepCount, treeHorizontalIndiceCount, treeHorizontalPattern)) 
        treeCurrentAnimation++; 
      break;
    case 4:
      if (animateTree(forward, on, treeAllGreensPatternStepCount, treeAllGreensIndiceCount, treeAllGreensPattern)) 
        treeCurrentAnimation++;
      break;
    case 5:
      if (animateTree(forward, on, treeAllRedsPatternStepCount, treeAllRedsIndiceCount, treeAllRedsPattern)) 
        treeCurrentAnimation++;
    case 6:
      if (animateTree(forward, off, treeRandomPatternStepCount, treeRandomIndiceCount, treeRandomPattern))
        treeCurrentAnimation++;
      break;
    case 7:
      if (animateTree(forward, on, treeVerticalInsideOutPatternStepCount, treeVerticalInsideOutIndiceCount, treeVerticalInsideOutPattern)) 
        treeCurrentAnimation++; 
      break;
    case 8:
      if (animateTree(forward, off, treeVerticalPatternStepCount, treeVerticalIndiceCount, treeVerticalPattern)) 
        treeCurrentAnimation++; 
      break;
    case 9:
      if (animateTree(forward, on, treeHorizontalPatternStepCount, treeHorizontalIndiceCount, treeHorizontalPattern)) 
        treeCurrentAnimation++; 
      break;
    case 10:
      if (animateTree(forward, off, treeVerticalInsideOutPatternStepCount, treeVerticalInsideOutIndiceCount, treeVerticalInsideOutPattern)) 
        treeCurrentAnimation++; 
      break;
    case 11:
      if (animateTree(reverse, on, treeHorizontalPatternStepCount, treeHorizontalIndiceCount, treeHorizontalPattern)) 
        treeCurrentAnimation++; 
      break;
    case 12:
      if (animateTree(reverse, off, treeVerticalInsideOutPatternStepCount, treeVerticalInsideOutIndiceCount, treeVerticalInsideOutPattern)) 
        treeCurrentAnimation++; 
      break;
    case 13:
      if (animateTree(forward, on, treeRandomPatternStepCount, treeRandomIndiceCount, treeRandomPattern))
        treeCurrentAnimation++;
      break;
    case 14:
      if (animateTree(forward, off, treeVerticalInsideOutPatternStepCount, treeVerticalInsideOutIndiceCount, treeVerticalInsideOutPattern)) 
        treeCurrentAnimation++; 
      break;
    default:
      treeCurrentAnimation = 0;
  }

  delayMicroseconds(animationDelay);
}


bool starsInitialized = false;
OnOff starGlowDirection[sizeOfStarIndices];
void glowStars() {
  if (!starsInitialized) {
    for (uint8_t idx = 0; idx < sizeOfStarIndices; idx++) {
      ledValue[starIndices[idx]] = random(50, 153);
      starGlowDirection[idx] = random(0, 100) % 2 == 0 ? on : off;
    }
    starsInitialized = true;
  }

  int8_t ledIndices[1];
  for (uint8_t idx = 0; idx < sizeOfStarIndices; idx++) {
    ledIndices[0] = starIndices[idx];
    OnOff stateLed = changeLedValues(starGlowDirection[idx], 1, ledIndices); 
    if (stateLed == _60) {
       starGlowDirection[idx] = off;
    } else if (stateLed == off) {
      starGlowDirection[idx] = on;
    }
  }
}

bool animateTree(Direction direction, OnOff onOff, uint8_t stepCount, uint8_t indiceCount, const void *pattern) {
  int8_t (*p_pattern)[stepCount][indiceCount] = (int8_t(*)[stepCount][indiceCount]) pattern;
  
  // Set the first animation step when no animation step is set.
  if (treeCurrentAnimationStep == -100) {
    treeCurrentAnimationStep = direction == forward ? 0 : (stepCount - 1);  
  }

  // Retrieve the current animation step indices
  int8_t *indices = (*p_pattern)[treeCurrentAnimationStep];

  int8_t ledIndices[indiceCount];
  for (uint8_t idx = 0; idx < indiceCount; idx++) {
    int8_t indice = indices[idx];
    if (indice != -1) {
      ledIndices[idx] = treeIndices[indices[idx]]; 
    } else { 
      ledIndices[idx] = -1;
    }
  }

  OnOff stateLed = changeLedValues(onOff, indiceCount, ledIndices);
  if (onOff == stateLed) {
    if (direction == forward) {
      treeCurrentAnimationStep++;
    } else if (direction == reverse) {
      treeCurrentAnimationStep--;
    }
  }

  if (treeCurrentAnimationStep == -1 || treeCurrentAnimationStep > stepCount) {
    treeCurrentAnimationStep = -100;
    return true;
  }

  return false;
}

/*
 * Increments or decrements the led values at the specified indices.
 */
OnOff changeLedValues(OnOff onOff, uint8_t sizeOfIndices, const int8_t indices[]) {
  int8_t indice = 0;
  for (uint8_t idx = 0; idx < sizeOfIndices; idx++) {
    indice = indices[idx];

    if (indice != -1) {
      if (onOff == on && stateLedValue(indices[idx]) != on) {
        ledValue[indices[idx]]++;
      } else if (onOff == off && stateLedValue(indices[idx]) != off)  {
        ledValue[indices[idx]]--;
      }
    }
  }

  return stateLedValue(indices[0]);
}

OnOff stateLedValue(uint8_t indice) {
  uint8_t value = ledValue[indice];
  return value == 255 ? on : value == 0 ? off : value == 51 ? _20 : value == 153 ? _60 : none;
}
