#include <SPI.h>

#define ENABLE_DEBUG 1

// The dimensions of the cube (how many RGB LED's in each direction)
#define CUBE_DIMENSION 8

// Each led has a bit depth of 8, which results in a 0-254 brightness
#define LED_BIT_DEPTH 8

// How many sub leds does a sinle LED have, for instance a normal LED has one,
// an RGB LED has 3.
#define LED_SUB_COUNT 3

// TLC5947 chipset specifications
#define TLC_COUNT         8
#define TLC_CHANNEL_COUNT 24
#define TLC_BIT_DEPTH     12

// Calculate the size of the SPI Buffer
#define SPI_BUFFER_SIZE (TLC_COUNT * TLC_CHANNEL_COUNT * TLC_BIT_DEPTH) / 8

// TLC5947 Wiring
//
// |- Function -|- Wire -|- ESP8266 -|- TLC PIN  -|
// | BLANK      | BLUE   | D3        | 2  (BLANK) |
// | XLAT       | GREEN  | D4        | 30 (XLAT)  | 
// | MOSI       | YELLOW | D5        | 4  (SIN)   |
// | CLOCK      | PURPLE | D7        | 3  (SCLK)  |
#define TLC_BLANK D3
#define TLC_XLAT  D4
#define SPI_CLOCK D5
#define SPI_MOSI  D7

// Timing measuring pins for the oscilloscope.
//#define LOOP_MEASURE_PIN  D1
#define TIMER_MEASURE_PIN D2
#define INTERRUPT_PIN     D1

// Index to keep track which layer we are displaying.
volatile byte anodeIndex = 0;

// Contains the brightness for each individual led, each value is 
// stored as bits (LED_BIT_DEPTH)
uint8_t cubeValues[CUBE_DIMENSION][LED_SUB_COUNT][CUBE_DIMENSION][CUBE_DIMENSION];

// The SPI buffer is used to temporarilty store the data that needs to be transmitted over the SPI bus.
// The size of the buffer is explained above (see SPI_BUFFER_SIZE)
uint8_t spiBuffer[SPI_BUFFER_SIZE];

// SPI Configuration
//
// Maximum speed:
// The frequency at which the data is transmitted, (Mhz), the maximum in 
// case of a single TLC5947 is 30-Mhz, when cascaded the maximum is 50%.
//
// Data output order:
// The data output order for the TLC5947 is according to the datasheet most significat bit first.
//
// Mode configuration:
// For the TLC5947 the SPI mode is 0, data is clocked in at the rising edge of the clock.
SPISettings TLC5947(15e6 , MSBFIRST, SPI_MODE0);

// Current active animation.
volatile uint8_t animationActive = 0;

// Available number of animations.
uint8_t animationCount = 2;

void ICACHE_RAM_ATTR onTimerISR(){
  digitalWrite(TIMER_MEASURE_PIN, HIGH);
  
  // The TLC5947 chips are daisy chained, which means that the first bit of data that is
  // send out ends up on the last TLC5947 chip (and then also for the last output). 
  // 
  // So when sending out the data we need to process the data in such a way that we send 
  // out the data in reverse order. Hence the spiBufferPos starts at the end of 
  // the buffer and works it way to the beginning.
  int spiBufferPos = SPI_BUFFER_SIZE - 1;

  // Fill the buffer with the data we need to send using the SPI bus. 
  // The cubeValues consists of a one byte value (0-255) so first convert it from 8bits (byte) to 12 bits (0-4095) (int) 
  // which consists of two bytes, only the twelve least significant bits are to be transmitted (since each output has a
  // 12 bit PWM signal).
  int8_t first = true;
  uint16_t data;
  
  for(int8_t colorIdx = 0; colorIdx < LED_SUB_COUNT; colorIdx++) {
    for (int8_t xIdx = 0; xIdx < CUBE_DIMENSION; xIdx++) {
      for (int8_t zIdx = 0; zIdx < CUBE_DIMENSION; zIdx++) {
        // We store the LED data as 8 bit values for convenience, but the TLC5947 expects 12bits
        // per output channel. So shift left 4 times to get a 16bit value (of which we will only use
        // the 12 least significant bits).
        data = cubeValues[anodeIndex][colorIdx][xIdx][zIdx] << 4;
    
        // Get the 8 least significant bits. 
        byte low = data & 0xff;
    
        if (first) {
          // first value
          spiBuffer[spiBufferPos] = low;
          spiBuffer[--spiBufferPos] = data >> 8;
          first = false;
        } else {
          // second value
          spiBuffer[spiBufferPos] = spiBuffer[spiBufferPos] | low << 4;
          spiBuffer[--spiBufferPos] =  data >> 4 | low >> 4;
          spiBufferPos--;
          first = true;
        }
      }
    }
  }

  //Start SPI transaction (disables interrupts)
  SPI.beginTransaction(TLC5947);

  // Transfer the complete buffer.
  SPI.transfer(&spiBuffer, SPI_BUFFER_SIZE);
  
  // Stop SPI transaction (enables interrupts again)
  SPI.endTransaction();
  
  digitalWrite(SPI_CLOCK, LOW);

  digitalWrite(TLC_BLANK, HIGH);
  digitalWrite(TLC_XLAT, HIGH);
  digitalWrite(TLC_XLAT, LOW);
  digitalWrite(TLC_BLANK, LOW);

  digitalWrite(TIMER_MEASURE_PIN, LOW);
}

void ICACHE_RAM_ATTR selectAnimation() {
  if (animationActive == 1) {
    animationActive = 0;
  } else {
    animationActive = 1;
  }
}


void setup() {
  #if ENABLE_DEBUG == true
    Serial.begin(74880);
  #endif

  // Set the pinmodes for all neccesary pins, XLAT must be LOW initially (is only used to
  // latch in the new data). CLOCK and MOSI are done through the SPI.begin() operation.
  pinMode(TLC_XLAT, OUTPUT);
  digitalWrite(TLC_XLAT, LOW);

  pinMode(TIMER_MEASURE_PIN, OUTPUT);

  // Disable timer interrupts.
  noInterrupts();

  // Setup the timer for sending the LED data to the TLC4947 IC's using SPI.
  //
  // TIM_DIV1      80MHz (80 ticks/us    - 104857.588 us max)
  // TIM_DIV16      5MHz (5 ticks/us     - 1677721.4  us max)
  // TIM_DIV256 312.5Khz (1 tick = 3.2us - 26843542.4 us max)
  // 
  // TIM_LOOP:   Recursive call to the interrupt
  // TIM_SINGLE: Single call to the interrupt
  timer1_attachInterrupt(onTimerISR);
  timer1_enable(TIM_DIV1, TIM_EDGE, TIM_LOOP);

  // Configure the timer to perform an interval of 512us.
  timer1_write(41000);

  // Enable timer interrupts.
  interrupts();

  pinMode(TLC_BLANK, OUTPUT);
  digitalWrite(TLC_BLANK, LOW);

  pinMode(INTERRUPT_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), selectAnimation, RISING);


  SPI.begin();
}

void loop() {
  if (animationActive == 0) {
    animateCompleteCubeToTargetValue(255);
  } else {
    animateCompleteCubeToTargetValue(0);
  }
}

uint8_t currentTargetValue = 0;
void animateCompleteCubeToTargetValue(uint8_t value) {
  if (currentTargetValue < value) {
    currentTargetValue++;
  } else if (currentTargetValue > value) {
    currentTargetValue--;
  }

  loopCompleteCube(functionToExecuteInLoopCompleteCube);
  
  delay(1);
}

void functionToExecuteInLoopCompleteCube(uint8_t yIdx, uint8_t colorIdx, uint8_t xIdx, uint8_t zIdx ) {
  cubeValues[yIdx][colorIdx][xIdx][zIdx] = currentTargetValue;
}

void loopCompleteCube(void (*func)(uint8_t, uint8_t, uint8_t, uint8_t)) {
  for (uint8_t yIdx = 0; yIdx < CUBE_DIMENSION; yIdx++) {
      for (uint8_t colorIdx = 0; colorIdx < LED_SUB_COUNT; colorIdx++) {
        for (uint8_t xIdx = 0; xIdx < CUBE_DIMENSION; xIdx++) {
          for (uint8_t zIdx = 0; zIdx < CUBE_DIMENSION; zIdx++) {
              (*func)(yIdx, colorIdx, xIdx, zIdx);
          }
        }      
      }
    }
}

void printCube() {
  #if ENABLE_DEBUG == true
    for (uint8_t yIdx = 0; yIdx < CUBE_DIMENSION; yIdx++) {
      Serial.println((String) "Layer: " + yIdx);
      for (uint8_t colorIdx = 0; colorIdx < LED_SUB_COUNT; colorIdx++) {
        for (uint8_t xIdx = 0; xIdx < CUBE_DIMENSION; xIdx++) {
          for (uint8_t zIdx = 0; zIdx < CUBE_DIMENSION; zIdx++) {
              uint8_t value = cubeValues[yIdx][colorIdx][xIdx][zIdx];
              Serial.print((String) (zIdx > 0 ? ", " : "") + value);
          }
        }      
      }
    }
 #endif
}
