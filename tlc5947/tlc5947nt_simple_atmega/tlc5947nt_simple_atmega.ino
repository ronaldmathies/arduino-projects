#include <SPI.h>

// TLC5947 chipset specifications
#define TLC_COUNT          1
#define TLC_CHANNEL_COUNT  24
#define TLC_BIT_DEPTH      12

// Number of channels actual in use
#define TLC_CHANNEL_IN_USE 6

// Data size for the leds.
#define LED_VALUE_SIZE TLC_COUNT * TLC_CHANNEL_COUNT

// Calculate the size of the SPI Buffer
#define SPI_BUFFER_SIZE (TLC_COUNT * TLC_CHANNEL_COUNT * TLC_BIT_DEPTH) / 8

// TLC Wiring
//
// |- Function -|- Wire -|- ATMEGA PIN -|- UNO PIN -|- TLC PIN  -|
// | XLAT       | GREEN  | 4            | 2         | 24 (XLAT)  | 
// | GSCLK      | WHITE  | 5            | 3         | 18 (GSCLK) | 
// | VPRG       | GRAY   | 6            | 4         | 27 (VPRG)  |
// | BLANK      | BLUE   | 11           | 5         | 23 (BLANK) |
// | SIN        | YELLOW | 19           | 11        | 26 (SIN)   |
// | CLOCK      | PURPLE | 17           | 13        | 25 (SCLKL) |
#define TLC_BLANK 5
#define TLC_XLAT  2
#define SPI_CLOCK 13
#define SPI_MOSI  11

#define SPI_CLOCK_PORTB 5  // SPI CLOCK ON PORTB

// Timing measuring pins for the oscilloscope.
#define TIMER_MEASURE_PIN 7
#define INTERRUPT_PIN     3

// Contains the brightness for each individual led, each value is 
// stored as bits (LED_BIT_DEPTH)
uint8_t ledValue[LED_VALUE_SIZE];

// The SPI buffer is used to temporarilty store the data that needs to be transmitted over the SPI bus.
// The size of the buffer is explained above (see SPI_BUFFER_SIZE)
uint8_t spiBuffer[SPI_BUFFER_SIZE];

// SPI Configuration
//
// Maximum speed:
// The frequency at which the data is transmitted, (Mhz), the maximum in 
// case of a single TLC5947 is 30-Mhz, when cascaded the maximum is 50%.
//
// Data output order:
// The data output order for the TLC5947 is according to the datasheet most significat bit first.
//
// Mode configuration:
// For the TLC5947 the SPI mode is 0, data is clocked in at the rising edge of the clock.
SPISettings TLC5947(16e6 , MSBFIRST, SPI_MODE0);

// Are the lights switched on or off.
volatile bool isOn = false;

// Value of LED to set in animation
uint8_t channelValue = 0;

// Channel index to update in animation.
uint8_t channelIdx = 0;

void setup() {
  Serial.begin(115200);

  // Set the pinmodes for all neccesary pins, XLAT must be LOW initially (is only used to
  // latch in the new data). CLOCK and MOSI are done through the SPI.begin() operation.
  pinMode(TLC_XLAT, OUTPUT);
  PORTD &= ~(1 << TLC_XLAT);

  pinMode(TIMER_MEASURE_PIN, OUTPUT);

  // Disable timer interrupts.
  noInterrupts();

  // Setup the timer for sending the LED data to the TLC4947 IC's using SPI.
  // Set TCCR1A and TCCR1B register to 0
  TCCR1A = 0;
  TCCR1B = 0;

  // Initialize counter value to 0
  TCNT1  = 0;
  
  // Set compare match register for 1953.125 Hz increments ( 512us )
  OCR1A = 8191; // = 16000000 / (1 * 1953.125) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12, CS11 and CS10 bits for 1 prescaler
  TCCR1B |= (0 << CS12) | (0 << CS11) | (1 << CS10);
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  // Enable timer interrupts.
  interrupts();

  pinMode(TLC_BLANK, OUTPUT);
  PORTD &= ~(1 << TLC_BLANK);

  pinMode(INTERRUPT_PIN, INPUT_PULLUP);

  // On / off switch interrupt
  // RISING: Only on the rising edge of the signal.
  // CHANGE: On the rising and falling edge of the signal.
  attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), switchOnOff, CHANGE);

  // Initialize SPI.
  SPI.begin();
}

volatile uint64_t lastSwitchAction = 0;

void switchOnOff() {
  if (millis() - lastSwitchAction > 250) {
    Serial.println(isOn ? "On" : "Off");
    isOn = !isOn;
    lastSwitchAction = millis();
  }
}

ISR(TIMER1_COMPA_vect) {
  // Write timer measure pin high for the ossciloscope.
  PORTD |= 1 << TIMER_MEASURE_PIN;

  // Fill the buffer with the data we need to send using the SPI bus. 
  // The ledvalue consists of a one byte value (0-255) so first convert it from 8bits (byte) to 12 bits (0-4095) (int) 
  // which consists of two bytes, only the twelve least significant bits are to be transmitted (since each output has a
  // 12 bit PWM signal).

  uint16_t data;
  uint16_t spiBufferPos = SPI_BUFFER_SIZE - 1; 
  int8_t first = true;
  for (int8_t idx = 0; idx < LED_VALUE_SIZE; idx++) {
    
    // We store the LED data as 8 bit values for convenience, but the TLC5947 expects 12bits
    // per output channel. So shift left 4 times to get a 16bit value (of which we will only use
    // the 12 least significant bits).
    data = ledValue[idx] << 4;

    // Get the 8 least significant bits. 
    byte low = data & 0xff;

    if (first) {
      // first value
      spiBuffer[spiBufferPos] = low;
      spiBuffer[--spiBufferPos] = data >> 8;
      first = false;
    } else {
      // second value
      spiBuffer[spiBufferPos] = spiBuffer[spiBufferPos] | low << 4;
      spiBuffer[--spiBufferPos] = data >> 4 | low >> 4;
      spiBufferPos--;
      first = true;
    }
  }
  
  // Submit data
  SPI.beginTransaction(TLC5947);
  SPI.transfer(&spiBuffer, SPI_BUFFER_SIZE);
  SPI.endTransaction();

  // Clock needs to be pulled low at the end of data
  // data transmission. Not sure why, datasheet
  // states that data on the SIN is clocked in on the rising edge and
  // appears on the SOUT on the falling edge.
  PORTB &= ~(1 << SPI_CLOCK_PORTB);

  // Latch in the data by pulling blank high, xlat high, xlat low and blank low.
  PORTD |= 1 << TLC_BLANK;
  PORTD |= 1 << TLC_XLAT;
  PORTD &= ~(1 << TLC_XLAT);
  PORTD &= ~(1 << TLC_BLANK);

  // Write timer measure pin low for the ossciloscope.
  PORTD &= ~(1 << TIMER_MEASURE_PIN);

}

void loop() {
  if (isOn) {
    if (channelValue < 255) {
      channelValue += 1;
    } else {
      if (channelIdx < TLC_CHANNEL_IN_USE) {
        channelIdx++;
        channelValue = 0;
      }
    }
  } else {
    if (channelValue > 0) {
      channelValue -= 1;
    } else {
      if (channelIdx > 0) {
        channelIdx--;
        channelValue = 255;
      }
    }
  }

  ledValue[channelIdx] = channelValue;

  delayMicroseconds(128);
}
