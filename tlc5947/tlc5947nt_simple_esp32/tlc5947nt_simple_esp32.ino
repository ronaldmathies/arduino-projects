#include <SPI.h>

// TLC5947 chipset specifications
#define TLC_COUNT          1
#define TLC_CHANNEL_COUNT  24
#define TLC_BIT_DEPTH      12

// Number of channels actual in use
#define TLC_CHANNEL_IN_USE 6

// Data size for the leds.
#define LED_VALUE_SIZE TLC_COUNT * TLC_CHANNEL_COUNT

// Calculate the size of the SPI Buffer
#define SPI_BUFFER_SIZE (TLC_COUNT * TLC_CHANNEL_COUNT * TLC_BIT_DEPTH) / 8

// TLC5947 Wiring
#define TLC_BLANK 26
#define TLC_XLAT  27
#define SPI_CLOCK 14
#define SPI_MOSI  13
#define SPI_MISO  12
#define SPI_SS    25

// SPI and timer
SPIClass *hspi = NULL;

volatile int interruptCounter;

// Time configuration
hw_timer_t * timer = NULL;

// Define portMUX_TYPE variable to take care of the synchronization between the
// main loop and the ISR, when modifying a shared variable.
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

// Timing measuring pins for the oscilloscope.
#define TIMER_MEASURE_PIN 33
#define INTERRUPT_PIN     32

// Contains the brightness for each individual led, each value is 
// stored as bits (LED_BIT_DEPTH)
uint8_t ledValue[LED_VALUE_SIZE];

// The SPI buffer is used to temporarilty store the data that needs to be transmitted over the SPI bus.
// The size of the buffer is explained above (see SPI_BUFFER_SIZE)
uint8_t spiBuffer[SPI_BUFFER_SIZE];

// SPI Configuration
//
// Maximum speed:
// The frequency at which the data is transmitted, (Mhz), the maximum in 
// case of a single TLC5947 is 30-Mhz, when cascaded the maximum is 50%.
//
// Data output order:
// The data output order for the TLC5947 is according to the datasheet most significat bit first.
//
// Mode configuration:
// For the TLC5947 the SPI mode is 0, data is clocked in at the rising edge of the clock.
SPISettings TLC5947(16e6 , MSBFIRST, SPI_MODE0);

// Are the lights switched on or off.
volatile bool isOn = false;

// Print buffer to serial port for display in serial monitor.
volatile bool printbuf = false;

// Value of LED to set in animation
uint8_t channelValue = 0;

// Channel index to update in animation.
uint8_t channelIdx = 0;

void IRAM_ATTR printBitsByte(byte n) {
  byte numBits = 8;  // 2^numBits must be big enough to include the number n
  char b;
  char c = ' ';   // delimiter character
  for (byte i = 0; i < numBits; i++) {
    // shift 1 and mask to identify each bit value
    b = (n & (1 << (numBits - 1 - i))) > 0 ? '1' : '0'; // slightly faster to print chars than ints (saves conversion)
    Serial.print(b);
    if (i < (numBits - 1) && ((numBits-i - 1) % 4 == 0 )) Serial.print(c); // print a separator at every 4 bits
  }
}

void IRAM_ATTR printSpiBuffer(uint8_t spiBuffer[]) {
  for (int idx = 0; idx < SPI_BUFFER_SIZE; idx++) {
    printBitsByte(spiBuffer[idx]);
    Serial.print(" - ");
  }
  Serial.println();
}

void IRAM_ATTR onTimerISR() {
  // Write timer measure pin high for the ossciloscope.
  digitalWrite(TIMER_MEASURE_PIN, HIGH);

  // Fill the buffer with the data we need to send using the SPI bus. 
  // The ledvalue consists of a one byte value (0-255) so first convert it from 8bits (byte) to 12 bits (0-4095) (int) 
  // which consists of two bytes, only the twelve least significant bits are to be transmitted (since each output has a
  // 12 bit PWM signal).

  uint16_t data;
  uint16_t spiBufferPos = SPI_BUFFER_SIZE - 1; 
  int8_t first = true;
  for (int8_t idx = 0; idx < LED_VALUE_SIZE; idx++) {
    
    // We store the LED data as 8 bit values for convenience, but the TLC5947 expects 12bits
    // per output channel. So shift left 4 times to get a 16bit value (of which we will only use
    // the 12 least significant bits).
    data = ledValue[idx] << 4;

    // Get the 8 least significant bits. 
    byte low = data & 0xff;

    if (first) {
      // first value
      spiBuffer[spiBufferPos] = low;
      spiBuffer[--spiBufferPos] = data >> 8;
      first = false;
    } else {
      // second value
      spiBuffer[spiBufferPos] = spiBuffer[spiBufferPos] | low << 4;
      spiBuffer[--spiBufferPos] = data >> 4 | low >> 4;
      spiBufferPos--;
      first = true;
    }
  }

  if (printbuf) {
    printSpiBuffer(spiBuffer);
    printbuf = false;
  }
  
  //Start SPI transaction (disables interrupts)
  hspi->beginTransaction(TLC5947);
  hspi->transfer(&spiBuffer, SPI_BUFFER_SIZE);
  hspi->endTransaction();

  // Clock needs to be pulled low at the end of data
  // data transmission. Not sure why, datasheet
  // states that data on the SIN is clocked in on the rising edge and
  // appears on the SOUT on the falling edge.
  digitalWrite(SPI_CLOCK, LOW);

  // Latch in the data by pulling blank high, xlat high, xlat low and blank low.
  digitalWrite(TLC_BLANK, HIGH);
  digitalWrite(TLC_XLAT, HIGH);
  digitalWrite(TLC_XLAT, LOW);
  digitalWrite(TLC_BLANK, LOW);

  // Write timer measure pin low for the ossciloscope.
  digitalWrite(TIMER_MEASURE_PIN, LOW);
}

void ICACHE_RAM_ATTR switchOnOff() {
  Serial.println(isOn ? "On" : "Off");
  isOn = !isOn;
}

void setup() {
  Serial.begin(74880);

  // Set the pinmodes for all neccesary pins, XLAT must be LOW initially (is only used to
  // latch in the new data). CLOCK and MOSI are done through the SPI.begin() operation.
  pinMode(TLC_XLAT, OUTPUT);
  digitalWrite(TLC_XLAT, LOW);

  pinMode(TIMER_MEASURE_PIN, OUTPUT);

  // Disable timer interrupts.
  noInterrupts();
  
  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &onTimerISR, true);
  timerAlarmWrite(timer, 41000, true);
  timerAlarmEnable(timer);

  // Enable timer interrupts.
  interrupts();

  pinMode(TLC_BLANK, OUTPUT);
  digitalWrite(TLC_BLANK, LOW);

  pinMode(INTERRUPT_PIN, INPUT_PULLUP);

  // On / off switch interrupt
  // RISING: Only on the rising edge of the signal.
  // CHANGE: On the rising and falling edge of the signal.
  attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), switchOnOff, RISING);

  // Initialize SPI.
  hspi = new SPIClass(HSPI);
  hspi->begin(SPI_CLOCK, SPI_MISO, SPI_MOSI, SPI_SS);

  // Slave select pin is not handled automatically, we
  // don't need it but for future reference, set it up anyway.
  pinMode(SPI_SS, OUTPUT);
}

void loop() {
  if (isOn) {
    if (channelValue < 255) {
      channelValue += 1;
    } else {
      if (channelIdx < TLC_CHANNEL_IN_USE) {
        channelIdx++;
        channelValue = 0;
      }
    }
  } else {
    if (channelValue > 0) {
      channelValue -= 1;
    } else {
      if (channelIdx > 0) {
        channelIdx--;
        channelValue = 255;
      }
    }
  }

  ledValue[channelIdx] = channelValue;

  delay(2);
}
