#include <FS.h> 
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <ESP8266HTTPClient.h>

#define LED                   D0 // Led indicator on the ESP12E
#define ONE_WIRE_BUS          D3 // One wire bus interface, set the jumper JP1 to correct GPIO port.
#define SETUP_PIN             D4 // Pin wich connects to the setup button to enable the portal.
#define TEMPERATURE_PRECISION 10 // Precision of the DS18B20 temperature sensor.
#define MAX_SENSOR_COUNT      36 // Maximum number of supported sensors (default 36, 12 for the main board, 24 for addition board)

WiFiManager wm;

// Setup a oneWire instance to communicate with the 
// DS18B20 temperature sensors.
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

// Variable holding the actual number of DS18B20 temperatur sensors.
volatile uint8_t actualSensorCount = 0;

// InfluxDB configuration parameters.
char influxdb_host[40] = "";
char influxdb_port[6]  = "8086";
char influxdb_db[40]   = "";
char influxdb_rp[40]   = "default";

// Identification names for each DS18B20 temperature sensor, used for communicating the
// values through to InfluxDB
char sensorIds[MAX_SENSOR_COUNT][40];

// The addresses of the detected DS18B20 temperature sensors.
DeviceAddress sensorAddresses[MAX_SENSOR_COUNT];

// Configuration parameters which will be displayed within the
// WifiManager portal interface.
WiFiManagerParameter influxdb_host_param("host", "InfluxDb Host", influxdb_host, 40);
WiFiManagerParameter influxdb_port_param("influxdbport", "InfluxDb Port", influxdb_port, 6);
WiFiManagerParameter influxdb_db_param("influxdbdb", "InfluxDb Database", influxdb_db, 40);
WiFiManagerParameter influxdb_rp_param("influxdbrp", "InfluxDb Retention Policy", influxdb_rp, 40);
WiFiManagerParameter *sensor_ids_param[MAX_SENSOR_COUNT];

uint8_t initialTemperaturesRequested = false;

// flag for saving data
bool shouldSaveConfig = false;

/**
 * Timer function that will read the DS18B20 sensors and submit the
 * results to the InfluxDB API. This function is called with a 10 second interval.
 */
void ICACHE_RAM_ATTR onTimerISR(){
   if (!initialTemperaturesRequested) {
    sensors.requestTemperatures(); 
    initialTemperaturesRequested = true;
   } else {
    float temp;
    char* buffer;

    for (uint8_t idx = 0; idx < actualSensorCount; idx++) {
      temp = sensors.getTempC(sensorAddresses[idx]);
      Serial.print("Sensor ");
      Serial.println(temp, DEC);
      postSensorData(sensorIds[idx], temp);
    }
    
    sensors.requestTemperatures(); 
   }
}

void postSensorData(char sensorId[40], float temperature) {
  HTTPClient http;
  String url = "http://" + String(influxdb_host) + ":" + String(influxdb_port) + "/write?db=" + String(influxdb_db) + "&rp=" + String(influxdb_rp);
  
  char buffer[10];
  dtostrf(temperature, 2, 1, buffer);
  
  String data = String(sensorId) + " value=" + String(buffer);
  if (http.begin(url)) {
    int httpCode = http.POST(data);    
  }
}

//callback notifying us of the need to save config
void saveConfigCallback () {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}

void setup() {
  Serial.begin(115200);
  Serial.setDebugOutput(true);  
  Serial.println("\nStarting");

  pinMode(SETUP_PIN, INPUT);
  
  discoverSensors();
  
  // TODO: laden configuratie overschijft nu de discover.
  loadConfiguration();

  wm.setSaveConfigCallback(saveConfigCallback);

  // Create the configuration parameters for each discovered DS18B20 sensor.
  for (uint8_t idx = 0; idx < actualSensorCount; idx++) {
    sensor_ids_param[idx] = new WiFiManagerParameter("sensor" +idx, "Sensor", sensorIds[idx], 40);
  }

  // Add all the configuration parameters to the WifiManager portal.
  wm.addParameter(&influxdb_host_param);
  wm.addParameter(&influxdb_port_param);
  wm.addParameter(&influxdb_db_param);
  wm.addParameter(&influxdb_rp_param);
  
  for (uint8_t idx = 0; idx < actualSensorCount; idx++) {
    wm.addParameter(sensor_ids_param[idx]);
  }
  
  std::vector<const char *> menu = {"wifi","info","param","sep","restart","exit"};
  wm.setMenu(menu);

  // Auto close configuration portal after 30 seconds
  wm.setConfigPortalTimeout(30);

  // Connect to the WiFi network, when no connection could be made then setup
  // an anonymous access point with the specified name.
  uint8_t result = wm.autoConnect("T-Sensor Board"); // anonymous ap
  if (!result) {
    Serial.println("Failed to connect or hit timeout");

    // If no connection was made or if the end-user did not enter the
    // configuration portal before the timout occured then reboot the
    // ESP to try again.
    ESP.restart();
  } 
  else {
    Serial.println("connected to WiFi network.");
  }

  if(shouldSaveConfig) {
    saveConfiguration();
  }

  Serial.println("Local IP:");
  Serial.println(WiFi.localIP());

  // Disable timer interrupts.
  noInterrupts();

  // Setup the timer for reating the temperatures
  //
  // TIM_DIV1      80MHz (80 ticks/us    - 104857.588 us max)
  // TIM_DIV16      5MHz (5 ticks/us     - 1677721.4  us max)
  // TIM_DIV256 312.5Khz (1 tick = 3.2us - 26843542.4 us max)
  // 
  // TIM_LOOP:   Recursive call to the interrupt
  // TIM_SINGLE: Single call to the interrupt
  timer1_attachInterrupt(onTimerISR);
  timer1_enable(TIM_DIV256, TIM_EDGE, TIM_LOOP);

  // Configure the timer to perform an interval of 10 seconds.
  timer1_write(3125000);

  // Enable timer interrupts.
  interrupts();
}

/**
 * Discover the DS18B20 sensors and retrieve the addresses.
 */
void discoverSensors() {
  sensors.begin();

  Serial.println("Locating devices... ");
  actualSensorCount = sensors.getDeviceCount();

  // locate devices on the bus
  Serial.print("Found ");
  Serial.print(actualSensorCount, DEC);
  Serial.println(" devices.");

  oneWire.reset_search();
  for (uint8_t idx = 0; idx < actualSensorCount; idx++) {
    if (!oneWire.search(sensorAddresses[idx])) {
      Serial.println("Unable to find address");
    } else {
      Serial.print("Address[");
      Serial.print(idx);
      Serial.print("] = ");
      for (uint8_t n=0; n<8; n++) {
         Serial.print(sensorAddresses[idx][n], HEX);
         Serial.print(" ");
      }
      Serial.print("\n");
    }
  };

  sensors.setResolution(TEMPERATURE_PRECISION);
  
  Serial.print("Finished locating devices.");
}

/**
 * Checks if the Setup button was pressed by the user, if so
 * then enable the config portal. If the button is pressed for
 * more then 3 seconds the clear out all the setting to factory default
 * and reboot the ESP.
 */
void checkSetupButton(){
  if ( digitalRead(SETUP_PIN) == LOW ) {
    delay(50);
    if( digitalRead(SETUP_PIN) == LOW ){
      Serial.println("Button Pressed");

      delay(3000); // reset delay hold
      if( digitalRead(SETUP_PIN) == LOW ){
        Serial.println("Resetting the the device to factory defaults and restart.");
        wm.resetSettings();
        ESP.restart();
      }
      
      // start portal with an 120 second timeout.
      Serial.println("Starting config portal");
      wm.setConfigPortalTimeout(120);
      
      if (!wm.startConfigPortal("T-Sensor Board")) {
        Serial.println("failed to connect or hit timeout");
        delay(3000);
        ESP.restart();
      } else {
        //if you get here you have connected to the WiFi
        Serial.println("connected to WiFi network.");
      }
    }
  }
}

bool loadConfiguration() {
  if (SPIFFS.begin()) {
    if (SPIFFS.exists("/sensor/config.json")) {
      File configFile = SPIFFS.open("/sensor/config.json", "r");
      if (!configFile) {
        Serial.println(F("Failed to open configuration /sensor/config.json for reading."));
        return false;
      }
    
      size_t size = configFile.size();
      if (size > 4096) {
        Serial.println(F("Configuration file is too large."));
        return false;
      }
    
      // Allocate a temporary JsonDocument
      // Don't forget to change the capacity to match your requirements.
      // Use arduinojson.org/v6/assistant to compute the capacity.
      StaticJsonDocument<4096> doc;
    
      // Deserialize the JSON document
      DeserializationError error = deserializeJson(doc, configFile);
      if (error) {
        Serial.println(F("Failed to read file, using default configuration"));
      }
    
      strlcpy(influxdb_host, doc["influxdb_host"] | "192.168.1.51", sizeof(influxdb_host));
      strlcpy(influxdb_db, doc["influxdb_db"] | "demo", sizeof(influxdb_db));
      strlcpy(influxdb_rp, doc["influxdb_rp"] | "standard", sizeof(influxdb_rp));
      strlcpy(influxdb_port, doc["influxdb_port"] | "standard", sizeof(influxdb_port));
    
      configFile.close();
    }
  } else {
    Serial.println(F("Failed to mount filesystem."));
  }
}

bool saveConfiguration() {
  if (SPIFFS.begin()) {

    // If a current configuration exists then remove it first.
    if (SPIFFS.exists("/sensor/config.json")) {
      SPIFFS.remove("/sensor/config.json");
    }
    
    File configFile = SPIFFS.open("/sensor/config.json", "w");
    if (!configFile) {
      Serial.println(F("Failed to open configuration /sensor/config.json for writing."));
      return false;
    }
  
    // Allocate a temporary JsonDocument
    // Don't forget to change the capacity to match your requirements.
    // Use arduinojson.org/assistant to compute the capacity.
    StaticJsonDocument<4096> doc;
  
    doc['influxdb_host'] = influxdb_host;
    doc['influxdb_db']   = influxdb_db;
    doc['influxdb_rp']   = influxdb_rp;
    doc['influxdb_port'] = influxdb_port;
  
    // Serialize JSON to file
    if (serializeJson(doc, configFile) == 0) {
      Serial.println(F("Failed to write to configuration /sensor/config.json"));
    }
  
    // Close the file
    configFile.close();
  } else {
    Serial.println(F("Failed to mount file system."));
  }
}


//String getParam(String name){
//  //read parameter from server, for customhmtl input
//  String value;
//  if(wm.server->hasArg(name)) {
//    value = wm.server->arg(name);
//  }
//  return value;
//}
//
//void saveParamCallback(){
//  Serial.println("[CALLBACK] saveParamCallback fired");
//  Serial.println("PARAM customfieldid = " + getParam("customfieldid"));
//}

void loop() {
  checkSetupButton();
  // put your main code here, to run repeatedly:
}
